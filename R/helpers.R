## helper functions
which_max <- function(.vec)
    .vec == max(.vec)

## helper functions
which_min <- function(.vec)
    .vec == min(.vec)

## fast unlist
un_list <- function(.l)
    unlist(.l, FALSE, FALSE)

## paste collapse
pasteC <- function(.str)
    paste(.str, collapse = "")

## split and unlist .str
ssplit <- function(.str)
    un_list(strsplit(.str, ""))

## convert vector of strings to list of vectors of chars
to_list <- function(.strs, .sep = "")
    strsplit(.strs, .sep)
