#include <Rcpp.h>
#include <algorithm>
#include <vector>

using namespace Rcpp;
// [[Rcpp::plugins(cpp11)]]

/* The stemming functions need to be on vectors of characters (std::string)
Because handling unicode with C++ is hard. It is easier to let R do the unicode
splitting, and then just use vectors here.
*/

//' Helper function to append 2 vectors without mutation
//' 
//' @param v1 Vector
//' @param v2 Vector
//' @export
//' @return A new vector c(v1, v2)
// [[Rcpp::export]]
std::vector<std::string> append_vecs(std::vector<std::string> v1,
				     std::vector<std::string> v2){
  int v1l = v1.size();
  int v2l = v2.size();
  int nvl = v1l+v2l;
  std::vector<std::string> nv(nvl);
  for(int j = 0; j<nvl; j++){
    if(j<v1l)
      nv[j] = v1[j];
    else
      nv[j] = v2[j-v1l];
  }
  return nv;
}

//' Helper function to append 2 vectors without mutation
//' 
//' @param v1 Vector
//' @param v2 Vector
//' @param v3 Vector
//' @export
//' @return A new vector c(v1, v2)
// [[Rcpp::export]]
std::vector<std::string> append_vecs2(std::vector<std::string> v1,
				      std::vector<std::string> v2,
				      std::vector<std::string> v3){
  auto nv = append_vecs(append_vecs(v1, v2), v3);
  return nv;
}

int min_length_strs (std::vector<std::vector<std::string>> vstr);

// [[Rcpp::export]]
std::vector<std::string>
lc_pref_cpp(std::vector<std::vector<std::string>> strs) {
  int min_length = min_length_strs(strs);
  std::vector<std::string> out_temp(min_length);
  bool flag = true;
  std::string start;
  int f_length = 0;
  for (int i = 0; i < min_length; i++) {
    flag = true;
    start = strs[0][i];
    for (int j = 1; j < strs.size(); j++) {
      if (strs[j][i] != start)
	flag = false;
    }
    if (flag) {
      out_temp[i] = start;
      f_length ++;
    }
    else
      break;
  }
  std::vector<std::string> out(out_temp.begin(),
			       out_temp.begin()+f_length);
  return out;
}

// [[Rcpp::export]]
std::vector<std::string>
lc_suff_cpp(std::vector<std::vector<std::string>> strs) {
  std::vector<std::vector<std::string>> strs_2 = strs;
  for(int i = 0; i < strs_2.size(); i++) {
    std::reverse(strs_2[i].begin(), strs_2[i].end());
  }
  std::vector<std::string> out = lc_pref_cpp(strs_2);
  std::reverse(out.begin(), out.end());
  return out;
}

// [[Rcpp::export]]
std::vector<std::vector<std::string>>
remove_pref_cpp(std::vector<std::vector<std::string>> strs,
		const std::vector<std::string> pref) {
  std::vector<std::vector<std::string>> out (strs.size());
  int pref_length = pref.size();
  for (int i = 0; i < strs.size(); i++) {
    std::vector<std::string> tmp(strs[i].begin() + pref_length,
				 strs[i].end());
    out[i] = tmp;
  }
  return out;
}

// [[Rcpp::export]]
std::vector<std::vector<std::string>>
remove_suff_cpp(std::vector<std::vector<std::string>> strs,
		const std::vector<std::string> suff) {
  std::vector<std::vector<std::string>> out (strs.size());
  int suff_length = suff.size();
  for (int i = 0; i < strs.size(); i++) {
    std::vector<std::string> tmp(strs[i].begin(),
				 strs[i].end() - suff_length);
    out[i] = tmp;
  }
  return out;
}


//////////////////////////////////////////////////////////////////////

//' Remove character from vector in position p
//'
//' Importantly, unlike R, this function counts from 0
//'
//' @param str String vector
//' @param p Position to remove from
//' @export
//' @return A string vector without the object in position p
//' @examples
//' rem_char_pos(c("a", "b", "c"), 1)
//' rem_char_pos(c("a", "b", "c"), 2)
// [[Rcpp::export]]
std::vector<std::string> rem_char_pos (std::vector<std::string> str, int p) {
  std::vector<std::string> res (str.size()-1);
  if (p < 0) stop("p cannot be less than 0.");
  if (p >= str.size()) stop("p cannot larger than str's length.");
  int j = 0;
  for (int i = 0; i < str.size(); i++){
    if(i != p) {
      res[j] = str[i];
      j++;
    }
  }
  return res;
}

//' Return all subseqeunces of str of -1 characters
//'
//' @param str String vector
//' @export
//' @return A vector of string vectors, where each member is a subsequence of `str`
//' @examples
//' string_succ(c("a", "b", "c", "d"))
//' string_succ(c("a", "b"))
//' string_succ(c("a"))
// [[Rcpp::export]]
std::vector<std::vector<std::string>> string_succ (std::vector<std::string> str) {
  if(str.size() == 1){
    std::vector<std::vector<std::string>> res (0);
    return res;
  } else{
    std::vector<std::vector<std::string>> res(str.size());
    for (int i = 0; i < str.size(); i++){
      res[i] = rem_char_pos(str, i);
    }
    return(res);
  }
}

//' Returns the minimum length of all strings in vstr
//'
//' @param vstr A list of string vectors
//' @export
//' @return An integer with the minimum length
// [[Rcpp::export]]
int min_length_strs (std::vector<std::vector<std::string>> vstr) {
  int l = vstr[0].size();
  for (int i = 0; i < vstr.size(); i++) {
    if (vstr[i].size() < l) {
      l = vstr[i].size();
    }
  }
  return l;
}

//' Apply string_succ to a vector of strings
//'
//' A list of string vectors with all substrings of the original strings.
//'
//' @param vstr A list of string vectors
//' @export
//' @return A list of string vectors
// [[Rcpp::export]]
std::vector<std::vector<std::string>> substrings
(std::vector<std::vector<std::string>> vstr) {
  int n = 0;
  for(int i = 0; i < vstr.size(); i++){ // calculate size of return vector
    n = n+vstr[i].size();
  }
  std::vector<std::vector<std::string>> res(n); // return vector
  // do each string
  int j = 0; // keep track of res vector
  for (int i = 0; i < vstr.size(); i++){
    std::vector<std::vector<std::string>> str_succ = string_succ(vstr[i]);
    for (int l = 0; l < str_succ.size(); l++){
      res[j] = str_succ[l];
      j = j+1;
    }
  }
  std::sort(res.begin(), res.end()); // make unique
  res.erase(std::unique(res.begin(), res.end()), res.end());
  return res;
}

//' Equalize all strings to length n
//'
//' It retunrs a list of string vectors, with all substrings of size n.
//'
//' @param vstr A list of string vectors
//' @export
//' @return A list of string vectors
// [[Rcpp::export]]
std::vector<std::vector<std::string>> eq_strs_to_n (std::vector<std::string> str, int n) {
  if (n > str.size()) {
    return {str};
  }
  std::vector<std::vector<std::string>> res = {str};
  for (int i = 0; i < str.size()-n; i++) {
    res = substrings(res);
  }
  return res;
}

//' Equalize all strings to length of shortest string
//'
//' It returns a list of string vectors, with all substrings of the same size.
//'
//' @param vstr A list of string vectors
//' @export
//' @return A list of string vectors
// [[Rcpp::export]]
std::vector<std::vector<std::vector<std::string>>>
eq_strings_to_min
(std::vector<std::vector<std::string>> vstr) {
  int min_l = min_length_strs(vstr);
  std::vector<std::vector<std::vector<std::string>>> res (vstr.size());
  for (int i = 0; i < vstr.size(); i++) {
    res[i] = eq_strs_to_n(vstr[i], min_l);
  }    
  return res;
}

//' Intersect two vectors of strings
//'
//' It returns the intersection of v1 and v2
//'
//' @param v1 A vector of strings
//' @param v2 A vector of strings
//' @export
//' @return A vector of strings
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
intersect_str_cpp (std::vector<std::vector<std::string>> v1,
		   std::vector<std::vector<std::string>> v2) {
  std::vector<std::vector<std::string>> v3;
  std::sort(v1.begin(), v1.end());
  std::sort(v2.begin(), v2.end());
  std::set_intersection(v1.begin(),v1.end(),
			v2.begin(),v2.end(),
			back_inserter(v3));
  return v3;
}

//' Intersect a list of vectors of strings
//'
//' This works like Reduce(intersect, list) in R
//' It returns the intersection of all vectors in `vstr`
//'
//' @param vstr A list of vectors of strings
//' @export
//' @return A vector of strings
// [[Rcpp::export]]
std::vector<std::vector<std::string>> intersect_strings
(std::vector<std::vector<std::vector<std::string>>> vstr) {
  std::vector<std::vector<std::string>> last_intersection = vstr[0];
  for (int i = 1; i < vstr.size(); ++i) {
    last_intersection =
      intersect_str_cpp(last_intersection, vstr[i]);
  }
  return last_intersection;
}

//' Apply substrings to all vectors in vstr
//'
//' This works like lapply(substrings, list) in R
//'
//' @param vstr A list of vectors of strings
//' @export
//' @return A list of vectors of strings
// [[Rcpp::export]]
std::vector<std::vector<std::vector<std::string>>>
substrings_list (std::vector<std::vector<std::vector<std::string>>> vstr) {
  std::vector<std::vector<std::vector<std::string>>> res(vstr.size());
  for (int i = 0; i < vstr.size(); i++){
    res[i] = substrings(vstr[i]);
  }
  return res;
}

//' Find the stem (longest common subsequence) of a vector of strings
//'
//' @param vstr A vector of strings
//' @export
//' @return A vector of strings
// [[Rcpp::export]]
std::vector<std::vector<std::string>>
find_stems_cpp (std::vector<std::vector<std::string>> vstr) {
  std::vector<std::vector<std::string>> res;
  std::vector<std::string> pref = lc_pref_cpp(vstr);
  std::vector<std::vector<std::string>> vstr_no_pref =
    remove_pref_cpp(vstr, pref);
  for (int i = 0; i < vstr_no_pref.size(); i++) {
    if (vstr_no_pref[i].size() == 0)
      return {pref};
  }
  std::vector<std::string> suff = lc_suff_cpp(vstr_no_pref);
  std::vector<std::vector<std::string>> vstr_no_suff =
    remove_suff_cpp(vstr_no_pref, suff);
  std::vector<std::vector<std::vector<std::string>>>
    start = eq_strings_to_min(vstr_no_suff);
  res = intersect_strings(start);
  while(res.size() == 0){
    start = substrings_list(start);
    res = intersect_strings(start);
  }
  // output we paste prefs and suffs
  std::vector<std::vector<std::string>> out(res.size());
  for(int i = 0; i < out.size(); i ++ ) {
    out[i] = append_vecs2(pref, res[i], suff);
  }
  return out;
}
