#include <Rcpp.h>

using namespace Rcpp;
// [[Rcpp::plugins(cpp11)]]

//' Helper function to append 2 vectors without mutation
//' 
//' @param v1 Vector
//' @param v2 Vector
//' @export
//' @return A new vector c(v1, v2)
// [[Rcpp::export]]
Rcpp::CharacterVector append_vecs_char(CharacterVector v1, CharacterVector v2){
  int v1l = v1.length();
  int v2l = v2.length();
  int nvl = v1l+v2l;
  CharacterVector nv(nvl);
  for(int j = 0; j<nvl; j++){
    if(j<v1l)
      nv(j) = v1(j);
    else
      nv(j) = v2(j-v1l);
  }
  return nv;
}

//' Generate levenshtein matrix for str1 and str2
//'
//' @param str1 String to align to
//' @param str2 String to align from
//' @export
//' @return A numeric matrix
// [[Rcpp::export]]
Rcpp::NumericMatrix lev_dist_cpp(Rcpp::StringVector str1,
				 Rcpp::StringVector str2){
  int lenStr1=str1.size();
  int lenStr2=str2.size();
  NumericMatrix d(lenStr1+1,lenStr2+1); // return matrix
  for(int i=0;i<lenStr1+1;i++)d(i,0)=i;
  for(int j=0;j<lenStr2+1;j++)d(0,j)=j;
  int  cost=0;
  NumericVector tmp(3);
  NumericVector tmp1(2);
  for(int i=1;i<lenStr1+1;i++){
    for(int j=1;j<lenStr2+1;j++){
      if(str1[i-1]==str2[j-1]) cost=0; // check if equal
      else cost=1;
      tmp[0]=d(i-1, j  ) + 1;    // left
      tmp[1]=d(i  , j-1) + 1;    // up
      tmp[2]=d(i-1, j-1) + cost; // diagonal
      d(i,j)=min(tmp);
    }
  }
  return d;
}

//' From a levenshtein matrix get the backtrace
//'
//' The backtrace is a matrix with the operations needed to go from `str2` to `str1`
//'
//' @param str1 String to align to
//' @param str2 String to align from
//' @export
//' @return A character matrix
// [[Rcpp::export]]
Rcpp::CharacterMatrix get_backtrace(Rcpp::StringVector str1,
				    Rcpp::StringVector str2){
  NumericMatrix m =  lev_dist_cpp(str1, str2); 
  int nr = m.nrow();
  int nc = m.ncol();
  CharacterMatrix dm(nr, nc); // return matrix
  // populate first row and column
  for (int c = 1; c<nc; c++)
    dm(0, c) = "l";
  for (int r = 1; r<nr; r++)
    dm(r, 0) = "u";
  dm(0,0) = "n";
  // populate the rest
  for (int c = nc-1; c>=1; c--){
    for (int r = nr-1; r>=1; r--){
      int minval = std::min(std::min(m(r, c-1), m(r-1, c)), m(r-1, c-1));
      String step;
      if (m(r, c-1) <= m(r,c))
	step = "l";
      if (m(r-1, c) <= m(r,c))
	step += "u";
      if ((str1.at(r-1) == str2.at(c-1)) && // check if we can go diagonally
	  (m(r-1, c-1) == m(r,c))) 
	step += "d";
      dm(r,c) = step;
      dm;
    }
  }
  return dm;
}

//' From a backtrace matrix, return all possible paths which solve the alignment
//'
//' This function only searchers for paths without insertion
//' This is because we assume the stem is fully contained in the form,
//' which means there are no need for insertions.
//' 
//' @param m Character Matrix with the backtrace.
//' @param op A string to use for separating the alignments.
//' @export
//' @return A vector with all alignments. The alignments are separated by op.
// [[Rcpp::export]]
Rcpp::CharacterVector get_alignments_cpp(CharacterMatrix m,
					 CharacterVector op){
  int nr = m.nrow();
  int nc = m.ncol();
  CharacterVector l = {"l"};
  CharacterVector d = {"d"};
  CharacterVector u = {"u"};
  if((nr==1) & (nc==1)) // base condition
    return op;
  else{ // otherwise do a recursive call
    if (m(nr-1,nc-1)=="u"){
      return CharacterVector::create("#");
    }
    if (m(nr-1,nc-1)=="lu"){ // only do l because we ignore insertions
      return get_alignments_cpp(m(_, Range(0, nc-2)), // move left
				append_vecs_char(l, op));
    }
    if (m(nr-1,nc-1)=="ud"){ // only do d because we ignore insertions
      return get_alignments_cpp(m(Range(0, nr-2), Range(0, nc-2)), // move diagonally
				append_vecs_char(d, op));
    }
    if (m(nr-1,nc-1)=="d"){ // d
      return get_alignments_cpp(m(Range(0, nr-2), Range(0, nc-2)), // move diagonally
				append_vecs_char(d, op));
    }
    if (m(nr-1,nc-1)=="l"){ // l
      return get_alignments_cpp(m(_, Range(0, nc-2)), // move left
				append_vecs_char(l, op));
    }
    if (m(nr-1,nc-1)=="ld") // l and d
      return append_vecs_char(get_alignments_cpp(m(Range(0, nr-2), // first d
						   Range(0, nc-2)),
						 append_vecs_char(d, op)),
			      get_alignments_cpp(m(_, Range(0, nc-2)), // then l
					    append_vecs_char(l, op)));
    if (m(nr-1,nc-1)=="lud") // l and d
      return append_vecs_char(get_alignments_cpp(m(Range(0, nr-2),
						   Range(0, nc-2)),
						 append_vecs_char(d, op)),
			      get_alignments_cpp(m(_, Range(0, nc-2)),
						 append_vecs_char(l, op)));
  }
}
