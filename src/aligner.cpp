#include <Rcpp.h>

using namespace Rcpp;
// [[Rcpp::plugins(cpp11)]]

//' Align a string to multiple paths
//' 
//' This function aligns a string to a path
//'
//' @param str String to align
//' @param paths Vector of paths for the alignment
//' @export
//' @return A vector of Aligned strings with the aligned stem and the markers
// [[Rcpp::export]]
std::vector<std::vector<std::vector<std::string>>>
align_strs_cpp(std::vector<std::string> str,
	       std::vector<std::vector<std::string>> paths) {
  std::vector<std::vector<std::vector<std::string>>>
    res (paths.size(),
	 std::vector<std::vector<std::string>>
	 (3, std::vector<std::string>
	  (str.size())));
  for (int j = 0; j < paths.size(); j++) {
    for(int i = 0; i < str.size(); i++) { // alignment
      if (paths[j][i] == "d") { 
	res[j][0][i] = str[i];
      } else {
	res[j][0][i] = "-";
      }
    }
    for(int i = 0; i < str.size(); i++) { // marker
      if (paths[j][i] == "l") { 
	res[j][1][i] = str[i];
      } else {
	res[j][1][i] = "-";
      }
    }
    res[j][2] = paths[j];
  }
  return res;
}
