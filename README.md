# Paradigma

This package performs automatic analysis of inflection paradigms.
It also has a function for multiple alignmenst.
The package is still under heavy development.

If you use this package, please cite: 

Matías Guzmán Naranjo and Laura Becker (forth.). Coding efficiency in nominal inflection: Expectedness and type frequency effects. 
in Linguistic Vanguard.

# Installation: 

``` R
devtools::install_gitlab("mguzmann89/paradigma")
```

# Usage

## For the paper "Coding efficnecy in nominal inflection: Expectedness and type frequency effects**

```R
df <- read_csv("./data/latin_N.csv")

df_2 <- df %>%
    mutate(form = str_remove_all(form, " ")) %>%
    pivot_wider(names_from = cell, values_from = form
              , values_fn = function(x) paste(x, collapse = ";")) %>%
    select(lexeme, `NOUN:Abl+Plur`:`NOUN:Voc+Sing`)

df_2

markers <- find_markers_paradigm(df_2, lexeme)
```

This should be much faster than doing the whole alignment but it might also be slightly worse.

## For the paper "Multiple alignments of inflectional paradigms"

```R
## for now we cannot use more than 1 character per phoneme
## so this is a workaround
## will fix some day
latin_n <- df %>%
    mutate(form_2 = form %>%
               str_replace_all("uː", "U") %>%
               str_replace_all("aː", "A") %>% 
               str_replace_all("oː", "O") %>% 
               str_replace_all("eː", "E") %>% 
               str_replace_all("iː", "I") %>% 
               str_replace_all("kʰ", "K") %>%
               str_replace_all("pʰ", "P") %>%
               str_replace_all("tʰ", "T") 
           )

align_paradigm <- paradigma::process_paradigms(latin_n, "Latin", "N")
```

# TODO:

- [] Allow multiple characters per phoneme
- [] Improve stem alignment
- [] Improve stem selection 
- [] Improve marker selection
- [] Implement iterative algorithms
